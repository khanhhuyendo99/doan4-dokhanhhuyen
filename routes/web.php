<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homeadmin');
});

// Route::group(['namespace' => 'Admin','prefix' => 'admin'], function(){

//     Route::get('/master', 'masterController@viewHome');

//});
Route::get('Loaisanpham','Loaisanphamcontroller@index')->name('getAllLoaisanpham');
Route::get('Loaisanpham/{id?}','Loaisanphamcontroller@edit')->name('getOneLoaisanpham');
Route::post('Loaisanpham','Loaisanphamcontroller@create')->name('getCreateLoaisanpham');
Route::put('Loaisanpham/{id?}','Loaisanphamcontroller@put')->name('getputLoaisanpham');
Route::delete('Loaisanpham','Loaisanphamcontroller@delete')->name('getDeleteLoaisanpham');


 Route::get('/','trangchuController@getIndex')->name('trangchu');

//   Route::get('/{type}','trangchuController@getloaisanpham');
   Route::get('/danhmuc/{id}','trangchuController@getloaisanpham')->name('danhmuc');

//  Route::get('/{id}','trangchuController@getsanphamchitiet');
   Route::get('/chi-tiet-sp/{id}',['as'=>'chitietsanpham','uses'=>'trangchuController@getsanphamchitiet']);

//  Route::get('/','trangchuController@getLienhe');
  Route::get('/lienhe',['as'=>'lienhe','uses'=>'trangchuController@getLienhe']);

//  Route::get('/','trangchuController@getGioithieu');
  Route::get('/gioithieu',['as'=>'gioithieu','uses'=>'trangchuController@getGioithieu']);


// Route::get('/','trangchuController@getDangnhap');
  Route::get('/dang-nhap',['as'=>'dangnhap','uses'=>'trangchuController@getDangnhap']);

  Route::get('/dang-ky',['as'=>'dangky','uses'=>'trangchuController@getDangky']);

  Route::post('/dang-ky',['as'=>'dangky','uses'=>'trangchuController@postDangky']);
