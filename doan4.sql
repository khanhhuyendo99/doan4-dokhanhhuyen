-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 21, 2020 lúc 11:07 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `doan4`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cthdb`
--

CREATE TABLE `cthdb` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_HDB` bigint(20) UNSIGNED NOT NULL,
  `id_SP` bigint(20) UNSIGNED NOT NULL,
  `SL` int(11) NOT NULL,
  `GiaBan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cthdn`
--

CREATE TABLE `cthdn` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_HDN` bigint(20) UNSIGNED NOT NULL,
  `id_SP` bigint(20) UNSIGNED NOT NULL,
  `SL` int(11) NOT NULL,
  `GiaNhap` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoadonban`
--

CREATE TABLE `hoadonban` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_KH` bigint(20) UNSIGNED NOT NULL,
  `NgayBan` datetime NOT NULL,
  `TrangThai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoadonnhap`
--

CREATE TABLE `hoadonnhap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_NCC` bigint(20) UNSIGNED NOT NULL,
  `NgayNhap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TrangThai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khachhang`
--

CREATE TABLE `khachhang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `HoTen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TaiKhoan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SĐT` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DiaChi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `khachhang`
--

INSERT INTO `khachhang` (`id`, `HoTen`, `TaiKhoan`, `Password`, `Email`, `SĐT`, `DiaChi`, `created_at`, `updated_at`) VALUES
(1, 'Đỗ Khánh Huyền', 'khanhhuyen99', 'huyen12345', 'khanhhuyen99@gmail.com', '0987623523', 'Hung Yen', NULL, NULL),
(2, 'Nguyễn Thị Hương', 'huong251999', 'huong1234', 'huong@gmail.com', '0349568789', 'Ha Noi', NULL, NULL),
(3, 'Lê Thị Trang', 'trangle1999', 'trang12345', 'trangle@gmail.com', '0234546576', 'Hai Duong', NULL, NULL),
(4, 'Đỗ Đức Mạnh', 'manhdo2000', 'manh20004', 'manhdo@gmail.com', '0987654321', 'Hung Yen', NULL, NULL),
(5, 'Nguyễn Thị Hà', 'hanguyen1999', 'ha12345', 'hannguyen@gmail.com', 'Hung Yen', '', NULL, NULL),
(6, 'Nguyễn Thị Linh', 'linh9998', 'linh12345', 'linhnguyen@gmail.com', '0987654321', 'Hai Phong', NULL, NULL),
(7, 'Nguyễn Thị Thùy', 'thuynghuyen99', 'thuy19999', 'thuynguyen@gmail.com', '09987654321', 'Hung Yen', NULL, NULL),
(8, 'Lê Thị Hằng', 'Hangle1999', 'Lehang12345', 'hangle@gmail.com', '0987654321', 'Ha Noi', NULL, NULL),
(9, 'Trần Thị Chi', 'chitran1999', 'chichi12345', 'chitran@gmail.com', '0987654321', 'Hung Yen', NULL, NULL),
(10, 'Hoàng Thị  nhung', 'nhung1998', 'nhung12345', 'nhunghoang@gmail.com', '0987654321', 'Hung Yen', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaisanpham`
--

CREATE TABLE `loaisanpham` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `TenLoai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MoTa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loaisanpham`
--

INSERT INTO `loaisanpham` (`id`, `TenLoai`, `MoTa`, `created_at`, `updated_at`) VALUES
(1, 'Đồ Bé Trai', 'Chất liệu đẹp bền phù hợp với mọi lứa tuổi', NULL, NULL),
(2, 'Đồ Bé Gái', 'Gía cả hợp lý phong cách trẻ trung năng động', NULL, NULL),
(3, 'Đồ Bộ', 'Họa tiết đẹp giúp cho các bé vui chơi thoải mái cách dễ dàng ', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2020_05_25_032033_create_sanpham_table', 1),
(8, '2020_05_25_032104_create_loaisanpham_table', 1),
(9, '2020_05_25_032138_create_khachhang_table', 1),
(10, '2020_05_25_032151_create_ncc_table', 1),
(11, '2020_05_25_032226_create_hoadonnhap_table', 1),
(12, '2020_05_25_032241_create_cthdn_table', 1),
(13, '2020_05_25_032304_create_hoadonban_table', 1),
(14, '2020_05_25_032323_create_cthdb_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ncc`
--

CREATE TABLE `ncc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `TenNCC` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DiaChi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SĐT` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ncc`
--

INSERT INTO `ncc` (`id`, `TenNCC`, `DiaChi`, `SĐT`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Thị Hường', 'Hung Yen', '0987654321', NULL, NULL),
(2, 'Hoàng THị Loan', 'Ha Noi', '0924556566', NULL, NULL),
(3, 'Trần Văn Cương', 'Hai Duong', '0983975485', NULL, NULL),
(4, 'Nguyễn Thị Hồng', 'Ha Noi', '0328947859', NULL, NULL),
(5, 'Hoàng Văn Đức', 'Hai Duong', '0987654321', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_loai` bigint(20) UNSIGNED NOT NULL,
  `TenSP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MoTa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Anh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `Gia` int(11) NOT NULL,
  `New` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`id`, `id_loai`, `TenSP`, `MoTa`, `Anh`, `SoLuong`, `Gia`, `New`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bộ cottong', ' Đồ bộ mặc nhà Blue seven được thiết kể theo dáng trẻ em Việt Nam, chất liệu 95% cotton và 5% spandex mang lại cảm giác thoải mái cho bé khi vận động và mềm mại, nhẹ nhàng khi đi ngủ.', 'anh1.jpg', 4, 40000, 1, NULL, NULL),
(2, 1, 'Quần bomber', 'Vải kaki luôn được các mẹ ưa chuộng, thường được may quần hoặc váy cho bé. Một chiếc kaki lửng cho bé trai phối với áo thun, áo sơ mi rất phong cách đặc biệt với các bé trai tròn một tí, diện quần kaki lửng trông múp míp rất đáng yêu các mẹ ạ.', 'anh2.jpg', 5, 100000, 1, NULL, NULL),
(3, 1, 'Áo khoác gió', 'Chất liệu thun visco cotton mát mẻ mềm mại đảm bảo bé thoải mái nhất khi mặc', 'anh3.jpg', 3, 70000, 1, NULL, NULL),
(4, 1, 'Áo Dài', 'Tuy không cầu kỳ như các bé ghé nhưng phong cách thời trang bé trai ấn tượng không kém gì các bé gái. Nếu mẹ biết chọn đồ phù hợp thì bé trai nhà mẹ trông rất phong cách đấy ạ. ', 'anh4.jpg', 6, 80000, 1, NULL, NULL),
(5, 1, 'Áo Cộc', 'với chiếc áo thun là bé có thể theo bố mẹ xuống phố đi chơi, đi tiệc với phong cách thời trang khỏe khoắn và năng động, hoặc khoác thêm chiếc áo gile bên ngoài là trông bé lịch lãm, đáng yêu vô cùng.', 'anh5.jpg', 2, 70000, 1, NULL, NULL),
(6, 1, 'Quần bò', 'Như đơn giản chỉ cần phối quần jeans cho bé trai với chiếc áo thun là bé có thể theo bố mẹ xuống phố đi chơi, đi tiệc với phong cách thời trang khỏe khoắn và năng động, hoặc khoác thêm chiếc áo gile bên ngoài là trông bé lịch lãm, đáng yêu vô cùng. ', 'anh6.jpg', 5, 20000, 1, NULL, NULL),
(7, 1, 'Quần gió', ' Quần size lớn cho bé từ 5 tuổi đến 9 tuổi mặc vừa vặn. Mẹ mua cho bé mặc rất đáng yêu mẹ nhé.', 'anh7.jpg', 5, 100000, 1, NULL, NULL),
(8, 1, 'Áo thun', 'mặc thoải mái. Màu nâu, màu kem, màu xanh, màu trắng dễ dàng mix với các kiểu áo thun, sơ mi để bé mặc đi chơi, đi tiệc', 'anh8.jpg', 9, 50000, 1, NULL, NULL),
(9, 1, 'Combo 5 quần', 'Quần size lớn cho bé từ 5 tuổi đến 9 tuổi mặc vừa vặn. Mẹ mua cho bé mặc rất đáng yêu mẹ nhé.', 'anh9.jpg', 1, 55000, 1, NULL, NULL),
(10, 2, 'Váy bò', 'Nhiều màu hồng, xanh đen, trắng hoa, trắng sao, kem chanh, vàng cầu vồng, họa tiết gần gũi. Kiểu đầm tay cánh tiên, dáng xòe. Size đại cho bé từ 4 tuổi đến 9 tuổi mặc dễ thương. Mẹ mua cho bé mặc hè mát mẻ, thoải mái nè mẹ ơi.', 'anh10.jpg', 6, 20000, 1, NULL, NULL),
(11, 2, 'Bộ áo váy', 'Đồ bộ mặc nhà cho bé gái 9 tháng đến 9 tuổi cân nặng 8.5kg đến 35kg in hình gầu dễ thương, bé mặc cặp với mẹ rất đáng yêu.', 'anh11.jpg', 4, 40000, 1, NULL, NULL),
(12, 2, 'Váy cánh tiên', 'bé gái áo tay ngắn cổ tròn form rộng thoải mái, in hình gấu dễ thương, quần legging lửng cùng tông màu, bé mặc rất đáng yêu. Chất vải thun cotton mềm mát mặc thoải dễ chịu. ', 'anh12.jpg', 5, 70000, 1, NULL, NULL),
(13, 2, 'Quần dài', 'Màu đỏ, vàng và xanh khá nổi bật. Size 9 tháng đến 9 tuổi mặc cặp với mẹ rất đáng yêu. Nào mẹ con cùng diện đồ xinh, trông 2 mẹ con đáng yêu lắm ạ.', 'anh14.jpg', 6, 20000, 1, NULL, NULL),
(14, 2, 'Váy xinh', 'Đầm voan cho bé gái 1 tuổi đến 8 tuổi mặc đi dự tiệc, đi chơi, đi lễ thêu hoa 3 D sang chảnh rất đáng yêu.', 'anh15.jpg', 5, 80000, 1, NULL, NULL),
(15, 2, 'Áó dài cách tân', 'Thân trên vải phi lót kate mặt trong, tùng 4 lớp: 2 lớp vol lưới bên trong, 1 lớp phi, 1 lớp kate, bé mặc thoải mái, xúng xính rất đáng yêu. Màu đỏ, vàng, hồng đào và hồng cam rực rỡ. Mẹ mua cho bé mặc dịp tết này rất hợp mẹ nhé.', 'anh16.jpg', 7, 55000, 1, NULL, NULL),
(16, 2, 'Combo 5 quần', 'Quần legging dài bé gái 3 tuổi đến 10 tuổi màu trơn đính dây ruy băng nhũ ánh kim tuyến, bé mặc phong cách, cá tính.', 'anh17.jpg', 7, 98000, 1, NULL, NULL),
(17, 2, 'Quần thu đông', 'Quần legging ôm dài, lưng thun, mix dây ruy băng nhũ vàng 2 bên sườn, in logo tròn trông rất sành điệu, cá tính. Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái', 'anh18.jpg', 5, 35000, 0, NULL, NULL),
(18, 2, 'Bộ sát nách', 'Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. size cho bé từ 15kg đến 45kg mặc vừa. Có cả size cho mẹ nữa ạ. Mẹ mua để mặc cặp cùng bé rất đáng yêu.', 'anh19.jpg', 5, 40000, 0, NULL, NULL),
(19, 2, 'Bộ đồ bơi', 'Chất liệu cottong mặc nhẹ mềm mại thoáng mát', 'anh21.jpg', 2, 80000, 0, NULL, NULL),
(20, 3, ' Bộ kẻ ', ' Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. size cho bé từ 15kg đến 45kg mặc vừa. ', 'anh22.jpg', 5, 100000, 0, NULL, NULL),
(21, 3, 'Bộ siêu nhân', 'Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái. Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. ', 'anh23.jpg', 2, 40000, 0, NULL, NULL),
(22, 3, 'Bộ kẻ đen', 'Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái. Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. ', 'anh25.jpg', 3, 100000, 0, NULL, NULL),
(23, 3, 'Bộ kẻ ngang', 'Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái. Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. ', 'anh26.jpg', 5, 70000, 0, NULL, NULL),
(24, 3, 'Bộ kẻ xanh', 'Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái. Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. ', 'anh27.jpg', 5, 50000, 0, NULL, NULL),
(25, 3, 'Bộ lụa', 'Kiểu dáng xinh xắn màu hồng đậm đễ nhìn giúp cho các bé rất thích', 'anh28.jpg', 4, 80000, 0, NULL, NULL),
(26, 3, 'Bộ sát nách', 'Vải thun cotton co giãn 4 chiều dễ thấm hút mồ hôi mặc thoải mái. Màu đen, xám trắng, xanh đậm dễ kết hợp với các kiểu áo cho bé mặc đi chơi, thể thao rất năng động. ', 'anh29.jpg', 1, 30000, 0, NULL, NULL),
(27, 3, 'Bộ con thỏ', 'Màu sắc dễ nhìn hình dáng phong phú giúp cho các bé tạo cảm giác thoải mái ', 'anh30.jpg', 5, 50000, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cthdb`
--
ALTER TABLE `cthdb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_hoadonban_cthdb` (`id_HDB`),
  ADD KEY `fk_sanpham_cthdb` (`id_SP`);

--
-- Chỉ mục cho bảng `cthdn`
--
ALTER TABLE `cthdn`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_hoadonnhap_cthdn` (`id_HDN`),
  ADD KEY `fk_sanpham_cthdn` (`id_SP`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hoadonban`
--
ALTER TABLE `hoadonban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_khachhang_hoadonban` (`id_KH`);

--
-- Chỉ mục cho bảng `hoadonnhap`
--
ALTER TABLE `hoadonnhap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ncc_hoadonnhap` (`id_NCC`);

--
-- Chỉ mục cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ncc`
--
ALTER TABLE `ncc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_loaisanpham_sanpham` (`id_loai`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cthdb`
--
ALTER TABLE `cthdb`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `cthdn`
--
ALTER TABLE `cthdn`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hoadonban`
--
ALTER TABLE `hoadonban`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hoadonnhap`
--
ALTER TABLE `hoadonnhap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `ncc`
--
ALTER TABLE `ncc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `cthdb`
--
ALTER TABLE `cthdb`
  ADD CONSTRAINT `fk_hoadonban_cthdb` FOREIGN KEY (`id_HDB`) REFERENCES `hoadonban` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sanpham_cthdb` FOREIGN KEY (`id_SP`) REFERENCES `sanpham` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `cthdn`
--
ALTER TABLE `cthdn`
  ADD CONSTRAINT `fk_hoadonnhap_cthdn` FOREIGN KEY (`id_HDN`) REFERENCES `hoadonnhap` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sanpham_cthdn` FOREIGN KEY (`id_SP`) REFERENCES `sanpham` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `hoadonban`
--
ALTER TABLE `hoadonban`
  ADD CONSTRAINT `fk_khachhang_hoadonban` FOREIGN KEY (`id_KH`) REFERENCES `khachhang` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `hoadonnhap`
--
ALTER TABLE `hoadonnhap`
  ADD CONSTRAINT `fk_ncc_hoadonnhap` FOREIGN KEY (`id_NCC`) REFERENCES `ncc` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `fk_loaisanpham_sanpham` FOREIGN KEY (`id_loai`) REFERENCES `loaisanpham` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
