@extends('layouts.app')
@section('content')
   <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Quản lý loại sp</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5   form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12  ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Danh sách loại sản phẩm</h2>
              <button class="btn btn-success" id="btn-add" name="btn-add"> Thêm loại sản phẩm </button>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Settings 1</a>
                      <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <tr>
                  <td>Mã loại</td>
                  <td>Tên loại</td>
                  <td>Mô tả</td>
                  <td>Sửa</td>
                  <td>Xóa</td>
                </tr>
                </thead>
                <tbody>
                  @isset($cates)
                  <?php
                   $dem=1;
                  ?>
                   @foreach($cates as $ca)
                   <tr id="{{ $ca->id }}">
                     <td>{{$dem++}}</td>
                     <td>{{$ca->TenLoai}}</td>
                     <td>{{$ca->MoTa}}</td>
                     <td>
                       <button class="btn btn-info open-modal" value="{{ $ca->id }}">
                            Sửa
                       </button>
                     </td>
                     <td>
                       <button class="btn btn-danger delete-ca" value="{{ $ca->id }}">
                            Xóa
                       </button>
                     </td>
                   </tr>
                   @endforeach
                   @endisset
                </tbody>
              <table>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="ShowModal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="ShowModalLabel">Link Editor</h4>
    </div>
    <div class="modal-body">
        <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">
            <div class="form-group">
                <label for="inputLink" class="col-sm-2 control-label">Ten loai</label>
                </br>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="tenloai" name="tenloai"
                           placeholder="Nhap vao ten" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="inputLink" class="col-sm-2 control-label">Mo Ta</label>
                </br>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="mota" name="mota"
                           placeholder="Nhap vao ten" value="">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes
        </button>
        <input type="hidden" id="ca_id" name="ca_id" value="0">
    </div>
</div>
</div>
</div>
@push('scripts')
<script>
$(document).ready(function($ca){
////----- Open the modal to CREATE a link -----////
jQuery('#btn-add').click(function () {
    jQuery('#btn-save').val("add");
    jQuery('#modalFormData').trigger("reset");
    jQuery('#ShowModal').modal('show');
});
////----- Open the modal to UPDATE a link -----////
jQuery('body').on('click', '.open-modal', function () {
    var ca_id = $(this).val();
    $.get('/Loaisanpham/' + ca_id, function (data) {
        jQuery('#ca_id').val(data.id);
        jQuery('#TenLoai').val(data.TenLoai_cate);
        jQuery('#MoTa').val(data.MoTa);
        jQuery('#btn-save').val("update");
        jQuery('#ShowModal').modal('show');
    })
});
// Clicking the save button on the open modal for both CREATE and UPDATE
$("#btn-save").click(function (e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    e.preventDefault();
    var ca_id = $('#ca_id').val();
    var formData = {
      name_cate: jQuery('#name').val(),
      description: jQuery('#note').val(),
    };
    var state = jQuery('#btn-save').val();
    var type = "POST";
    var link_id = jQuery('#ca_id').val();
    var ajaxurl = '{{ url("/category") }}';
    if (state == "update") {
        type = "PUT";
        ajaxurl = '{{ url("/category") }}' +'/'+ ca_id;
    }
    $.ajax({
        type: type,
        url: ajaxurl,
        data: formData,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            var link = '<tr id="ca_id' + data.id + '"><td>' + data.id + '</td><td>' + data. name_cate + '</td><td>' + data.description + '</td>';
            link += '<td><button class="btn btn-info open-modal" value="' + data.id + '">Edit</button></td>';
            link += '<td><button class="btn btn-danger delete-ca" value="' + data.id + '">Delete</button></td></tr>';
            if (state == "add") {
                jQuery('#ca-list').append(link);
            } else {
                $("#ca" + ca_id).replaceWith(link);
            }
            jQuery('#modalFormData').trigger("reset");
            jQuery('#ShowModal').modal('hide')
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
});
////----- DELETE a link and remove from the page -----////
jQuery('.delete-ca').click(function () {
    var ca_id = $(this).val();
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
       }
    });
    $.ajax({
        type: "DELETE",
        dataType: 'json',
        url: '/category/' + ca_id,
        success: function (data) {
            console.log(data);
            $("#ca" + ca_id).remove();
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
});
});
</script>

<!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
  @endpush







            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
