 <!-- Blog Section Begin -->
@extends('hometrangchu')
@section('content')
<section class="blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="blog__sidebar">
                    <div class="blog__sidebar__search">

                    </div>
                    <div class="blog__sidebar__item">
                        <h4>CÁC SẢN PHẨM</h4>
                         <ul>
                            <li><a href="#">Đồ Bé Trai</a></li>
                            <li><a href="#">Đồ Bé Gái</a></li>
                            <li><a href="#">Đồ Ngủ</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="row">
                    @foreach($sp_theoloai as $sp)
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="{{ URL::asset('trangchu/image/' . $sp->Anh)}}" alt="">
                            </div>
                            <div class="blog__item__text">
                                <h5><a href="#">{{$sp->TenSP}}</a></h5>
                                <p>{{$sp->MoTa}}</p>
                                <a href="#" class="blog__btn">Xem chi tiết <span class="arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                    <div class="col-lg-12">
                        <div class="product__pagination blog__pagination">
                            <a href="#">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->
@endsection
