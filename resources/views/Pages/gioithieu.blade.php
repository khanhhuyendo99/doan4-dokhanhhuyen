@extends('hometrangchu')
@section('content')
 <!-- Product Details Section Begin -->

<!-- Product Details Section End -->

<!-- Related Product Section Begin -->
<section class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related__product__title">
                    <h2>Giới thiệu</h2>
                </div>
            </div>
        </div>
         <div class="row">
                    <a href="./index.html"><img  src="{{URL::asset('trangchu/image/sile8.jpg')}}" alt=""></a>
                        <p>Vinakids là nhãn hiệu thời trang trẻ em cao cấp đã được đăng ký bảo hộ nhãn hiệu hàng hóa tại Cục Sở Hữu Trí Tuệ Việt Nam.
                            Với mong muốn mang đến những sản phẩm may mặc hàng Việt Nam chất lượng, đặc biệt là an toàn cho sức khỏe của trẻ nhỏ.
                            Lấy cảm hứng từ nhu cầu thực tế của người tiêu dùng Việt Nam, Vinakids cho ra những mẫu sản phẩm vừa lạ, vừa độc đáo vừa hợp thời trang phù hợp với xu hướng thời trang trong nước và quốc tế,dành cho các bé từ 1-12 tuổi..</p>
                <br> <p> <b>Lý do bạn chọn Vinakids</b>
                    <br>

                    1. Vinakids là nhãn hiệu đã được đăng ký bảo hộ độc quyền trên toàn quốc, tất cả các sản phẩm chúng tôi làm ra đều được gắn nhãn hiệu Vinakids riêng không sử dụng nhãn mác của các thương hiệu khác.
                     <br>
                    2. Chất liệu vải 100% cotton từ sợi bông tự nhiên, an toàn cho da của trẻ em và thân thiện với môi trường.
                       <br>
                    3. Hình in, thêu, dệt bằng công nghệ mới nhất giúp bạn giặt máy thoải mái không lo bong chóc.
                    <br>
                    4. Độ bền sản phẩm cao từ 3-10 năm sử dụng
                   <br>
                    5. Chúng tôi có khả năng cung cấp đầy đủ sản phẩm cho 1 shop quần áo mới mở vì chúng tôi luôn có sẵn sản phẩm trong kho phục vụ bạn với các mẫu mã mới ra liên tục (trung bình 5 mẫu / tuần)
                 <br>
                    6. Tư vấn miễn phí 24/7 và giải quyết thắc mắc trong vòng 15 phút kể từ khi nhận được yêu cầu
                  <br>
                    7. Chúng tôi luôn có sản phẩm tặng kèm cho khách mua buôn và cho đại lý dù đơn hàng nhiều hay ít đều có quà tặng đi kèm.
                   <br>
                    8. Tất cả các sản phẩm Tại Vinakids được sản xuất tại Việt Nam và chất liệu vải được Vinakids lựa chọn kỹ càng trước khi sản xuất.
                  <br>
                      Chúng tôi đã và đang không ngừng hoàn thiện mẫu mã và chất lượng sản phẩm để góp phần tạo dựng thương hiệu hàng Việt Nam vươn ra thị trường quốc tế.
                    Hiện nay chúng tôi đang tuyển đại lý quần áo trẻ em trên toàn quốc với rất nhiều chính sách hấp dẫn.
                    </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Related Product Section End -->
    @endsection
