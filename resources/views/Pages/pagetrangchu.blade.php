@extends('hometrangchu')
@section('content')
  <!-- Categories Section Begin -->
<!-- Categories Section End -->

<!-- Featured Section Begin -->

<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="beta-products-list">
                    <h2>Sản phẩm nổi bật</h2>
                    <div class="beta-products-details">
                        <p class="pull-left"> Tìm thấy {{count($new_spnoibat)}} sản phẩm</p>
                    </div>
                </div>
                <div class="featured__controls">
                </div>
            </div>
        </div>
        <div class="row featured__filter">
            @foreach($new_spnoibat as $new)
            <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
                <div class="featured__item">
                <div class="featured__item__pic set-bg">
                    <img src="trangchu/image/{{$new->Anh}}" alt="" />
                        <ul class="featured__item__pic__hover">
                            <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                            <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="featured__item__text">
                    <h6><a href="/chi-tiet-sp/{{$new->id}}">{{$new->TenSP}}</a></h6>
                    <h5>{{$new->Gia}}</h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">{{$new_spnoibat->links()}}</div>
    </div>
</section>
<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="beta-products-details">
                    <h2>Sản phẩm khác </h2>
                    <div class="beta-products-details">
                        <p class="pull-left">Tìm thấy {{count($new_spnoibat)}} sản phẩm</p>
                    </div>
                </div>
                <div class="featured__controls">
                </div>
            </div>
        </div>
        <div class="row featured__filter">
            @foreach($new_spkhac as $new)
            <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
                <div class="featured__item">
                <div class="featured__item__pic set-bg">
                    <img src="trangchu/image/{{$new->Anh}}" alt="" />
                        <ul class="featured__item__pic__hover">
                            <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                            <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="featured__item__text">
                    <h6><a href="/chi-tiet-sp/{{$new->id}}">{{$new->TenSP}}</a></h6>
                    <h5>{{$new->Gia}}</h5>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="row">{{$new_spkhac->links()}}</div>
        </div>
    </div>
</section>
<!-- Featured Section End -->

 <!-- Banner Begin -->
 <div id="owl-demo-ppsay">
    <div class="container">
         <div class="ppsay-title"><h4>PHẢN HỒI CỦA KHÁCH HÀNG</h4></div>
        <div class="carousel-inner hidden-sm hidden-xs">

                <div class="item active">
                    <div class="ppsaydiv">
                        <div class="row">
                        <div class="ppsaycontent hidden-xs col-sm-6 col-md-6">
                            <div class="ppsayavt col-md-3 col-sm-3">
                            <a href="./index.html"><img style="width:200px;height:100px;" src="{{URL::asset('trangchu/image/sile9.jpg')}}" alt="Ms. Huong Nguyen"></a>
                </div>
                 <div class="">
                                <div class="ppsaycontentcmt">
                                    <p style="text-align: justify"><span>“ </span>Quần áo tốt chất lượng giá cả phải chăng tôi rất hài lòng khi đặt hàng tại shop tôi sẽ ủng hộ dài. <span>”</span></p>
                                </div>
                                <p><span>Customer:</span> <span class="name">Ms. Huyen Do</span></p>
                            </div>

        </div>
        <div class="ppsaycontent hidden-xs col-sm-6 col-md-6">
            <div class="ppsayavt col-md-3 col-sm-3">
    <a href="./index.html"><img style="width:200px;height:100px;" src="{{URL::asset('trangchu/image/sile8.jpg')}}" alt="Ms. Huong Nguyen"></a>
</div>
 <div class="">
                <div class="ppsaycontentcmt">
                    <p style="text-align: justify"><span>“ </span>Quần áo tốt chất lượng giá cả phải chăng tôi rất hài lòng khi đặt hàng tại shop tôi sẽ ủng hộ dài. <span>”</span></p>
                </div>
                <p><span>Customer:</span> <span class="name">Ms. Huong Nguyen</span></p>
            </div>

</div>
</div>

<!-- Banner End -->
<!--PEOPLE SAY-->
<style>
    .carousel-inner .item active{
        float: ri;
    }
    .ppsay-title {
    text-align: center;
}
</style>


<!--END PEOPLE SAY-->
@endsection
