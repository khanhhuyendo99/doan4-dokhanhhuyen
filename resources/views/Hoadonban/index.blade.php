@extends('Admin.layout')
@section('title', 'Danh sách đơn hàng mới')
@section('header_action', 'Danh sách đơn hàng mới')
@section('header', 'Đơn hàng')
@section('nav_bar')
    <li class="nav-item d-none d-sm-inline-block active">
        <a href="index3.html" class="nav-link">Danh sách đơn hàng mới</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block active">
        <a href="index3.html" class="nav-link">Danh sách đã xử lý</a>
    </li>
@endsection

@section('content')
<style>
    th{
      text-align: center;
    }
    td img{
      width:80px;height:80px;margin-left:25px;

    }
  </style>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                {{--          <div class="card-header">--}}
                {{--            <h3 class="card-title">DataTable with minimal features & hover style</h3>--}}
                {{--          </div>--}}
                <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Mã Đơn Hàng</th>
                                <th>Tên Sản Phẩm</th>
                                <th style="width:300px;">Mô tả</th>
                                <th>Hình ảnh</th>
                                <th>Giá</th>
                                <th colspan="3">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$Order->isEmpty())
                                @foreach ($Order as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->describe}}</td>
                                        <td><img src="{{ URL::asset('admins/img/' .$order->image)}}"/></td>
                                        <td>{{$order->price}}</td>
                                        <td>Thêm</td>
                                        <td>Sửa</td>
                                        <td>Xóa</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" class="text-center">Chưa có đơn hàng mới nhé Goođ Luck ! ^.0</td>
                                </tr>
                            @endif

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>
@endsection
