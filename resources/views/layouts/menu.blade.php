<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i>Quản lý loại Sản Phẩm <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="index.html">Danh sách sản phẩm</a></li>
            <li><a href="index2.html">Thống kê</a></li>
          </ul>
        </li>
        <li><a><i class="fa fa-edit"></i> Quản lý sản phẩm <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="form.html">Danh sách sản phẩm</a></li>
            <li><a href="form_advanced.html">Thêm</a></li>
            <li><a href="form_validation.html">Sửa</a></li>
            <li><a href="form_wizards.html">Xóa</a></li>
          </ul>
        </li>
        <li><a><i class="fa fa-desktop"></i>Quản lý hóa đơn nhập<span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="general_elements.html">Chi tiết hóa đơn nhập</a></li>
          </ul>
        </li>
        <li><a><i class="fa fa-table"></i> Quản lý hóa đơn bán<span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="tables.html">Chi tiết hóa đơn bán </a></li>

          </ul>
        </li>
        <li><a><i class="fa fa-bar-chart-o"></i> Quản Lý Khách hàng<span class="fa fa-chevron-down"></span></a>
        </li>
        <li><a><i class="fa fa-clone"></i>Quản lý nhà cung cấp <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
            <li><a href="fixed_footer.html">Fixed Footer</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="menu_section">
      <h3></h3>

        </li>
        <li><a><i class="fa fa-windows"></i> <span class="fa fa-chevron-down"></span></a>

        </li>
        <li><a><i class="fa fa-sitemap"></i>  <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">

          </ul>
        </li>
        <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i>  <span class="label label-success pull-right"></span></a></li>
      </ul>
    </div>

  </div>
