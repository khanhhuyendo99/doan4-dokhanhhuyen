<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Website bán quần áo</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
     <link rel="stylesheet" href="{{asset('trangchu/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/font-awesome.min.css')}}" type="text/css">
     <link rel="stylesheet" href="{{asset('trangchu/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('trangchu/css/css.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('https://www.w3schools.com/w3css/4/w3.css')}}">
    <style>
        .mySlides {display:none;}
        </style>

</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> khanhhuyen@gmail.com</li>
                                <li>Free Shipping for all Order of $99</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                            <div class="header__top__right__language">
                                <!--<img src="{{ URL::asset('img/language.png')}}" alt=""> -->
                                <div>VietNam</div>
                                <span class="arrow_carrot-down"></span>
                            </div>
                            <div class="header__top__right__auth">
                                <a href="#"><i class="fa fa-user"></i> Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="#"><img style="height: 70px;" src="{{URL::asset('trangchu/image/logo.jpg')}}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <nav class="header__menu">
                        <ul>
                        <li class="active"><a href="{{route('trangchu')}}">Trang chủ</a></li>
                            <li><a href="#">Loại sản phẩm</a>
                                <ul class="header__menu__dropdown">
                                    @foreach($loai_sp as $loai)

                                <li><a href="{{route('danhmuc',$loai->id)}}">{{$loai->TenLoai}}</a></li>
                                    @endforeach
                                </ul>
                           </li>
                            <li><a href="{{route('gioithieu')}}">GIới thiệu</a></li>
                            <li><a href="{{route('lienhe')}}">Liên hệ</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                        </ul>
                        <div class="header__cart__price">item: <span>$150.00</span></div>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Danh mục </span>
                        </div>
                        <ul>
                            <li><a href="#">Đồ bé trai</a></li>
                            <li><a href="#">Đồ bé gái</a></li>
                            <li><a href="#">Đồ Bộ</a></li>
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                {{-- <div class="hero__search__categories">

                                    <span class="arrow_carrot-down"></span>
                                </div> --}}
                                {{-- <input type="text" placeholder="What do yo u need?"> --}}
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+65 11.188.888</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" style="width:100%">
                        <img class="mySlides" style="width:100%;height:100%" src="{{URL::asset('trangchu/image/sile7.jpg')}}" alt="">
                        <img class="mySlides" style="width:100%;height:100%" src="{{URL::asset('trangchu/image/sile8.jpg')}}" alt="">
                        <img class="mySlides" style="width:100%;height:100%" src="{{URL::asset('trangchu/image/sile9.jpg')}}" alt="">
                        {{-- <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                        <button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button> --}}
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->


    @yield('content')
    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="trangchu/img/logo.jpg" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: 16-Long Biên-Hà Nội</li>
                            <li>Phone: +84-98765432</li>
                            <li>Email: khanhhuyen@gmail.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Liên kết hữu ích</h6>
                        <ul>
                            <li><a href="#">Về chúng tôi</a></li>
                            <li><a href="#">Về cửa hàng của chúng tôi</a></li>
                            <li><a href="#"> Mua sắm an toàn</a></li>
                            <li><a href="#">Thông tin giao hàng</a></li>
                            <li><a href="#">Chính sách bảo mật</a></li>
                            <li><a href="#">Sơ đồ trang web của chúng tôi</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Chúng ta là ai</a></li>
                            <li><a href="#">Dịch vụ của chúng tôi</a></li>
                            <li><a href="#">Dự án</a></li>
                            <li><a href="#">Tiếp xúc</a></li>
                            <li><a href="#">Sự đổi mới</a></li>
                            <li><a href="#">Lời chứng thực</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Tham gia Bản tin của chúng tôi ngay
                            </h6>
                        <p> Nhận cập nhật E-mail về cửa hàng mới nhất của chúng tôi và ưu đãi đặc biệt.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Lời chứng thực</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script>Đã đăng ký Bản quyền | Mẫu này được thực hiện với <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
     <script src="{{asset('trangchu/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('trangchu/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('trangchu/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('trangchu/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('trangchu/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('trangchu/js/mixitup.min.js')}}"></script>
    <script src="{{asset('trangchu/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('trangchu/js/main.js')}}"></script>
    <script>
        var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";
}
    </script>



</body>

</html>
