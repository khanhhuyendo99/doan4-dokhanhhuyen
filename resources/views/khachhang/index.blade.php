@extends('Admin.layout')
@section('title', 'Danh sách người dùng')
@section('header_action', 'Danh sách người dùng ')
@section('header', 'Người dùng')

@section('nav_bar')
    <li class="nav-item d-none d-sm-inline-block active">
        <a href="index3.html" class="nav-link">Danh sách người dùng</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block active">
        <a href="" class="nav-link">Chỉnh sửa người dùng</a>
    </li>
@endsection

@section('content')
<style>
    th{
      text-align: center;
    }
    td img{
      width:80px;height:80px;margin-left:25px;

    }
  </style>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                {{--          <div class="card-header">--}}
                {{--            <h3 class="card-title">DataTable with minimal features & hover style</h3>--}}
                {{--          </div>--}}
                <!-- /.card-header -->
                    <div class="card-body">
                        <table id="postTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Mã người dùng</th>
                                <th>Tên người dùng</th>
                                <th style="width:300px;">Địa chỉ</th>
                                <th style="width:300px;">Số điện thoại</th>
                                <th colspan="3">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$lstUser->isEmpty())
                                @foreach ($lstUser as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->address}}</td>
                                        <td>{{$user->mobile}}</td>
                                        <td>Sửa</td>
                                        <td>Xóa</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" class="text-center">Chưa có người dùng hãy tạo thêm nhé Goođ Luck ! ^.0</td>
                                </tr>
                            @endif

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>
@endsection
