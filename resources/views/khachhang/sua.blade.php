@extends('Admin.layout')
@section('title', 'Sửa người dùng')
@section('menu_name', 'Người dùng')
@section('menu_action', 'Sửa người dùng')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">&nbsp;</h3>

                    <div class="card-tools">
                        {{--                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i--}}
                        {{--                                    class="fas fa-minus"></i></button>--}}
                        {{--                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i--}}
                        {{--                                    class="fas fa-times"></i></button>--}}
                    </div>
                </div>
                <form role="form" method="POST" action="/admin/user/edit">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_fullname">Tên người dùng</label>
                                    <input type="text" class="form-control" id="user_fullname" name="user_fullname"
                                           placeholder="Nhập tên người dùng"/>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="user_name">Tên tài khoản</label>
                                    <input type="text" class="form-control" id="user_name" name="user_name"
                                           placeholder="Nhập tên tài khoản"/>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="user_email">Tên email</label>
                                    <input type="email" class="form-control" id="user_email" name="user_email"
                                           placeholder="Nhập email"/>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="isActive">Trạng thái</label>
                                    <select class="form-control select2" style="width: 100%;"
                                            data-placeholder="Chọn trạng thái" name="isActive" id="isActive">
                                        <option value="1" selected="selected">Hoạt động</option>
                                        <option value="0">Không hoạt động</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="user_tel">Số điện thoại</label>
                                    <input type="text" class="form-control" id="user_tel" name="user_tel"
                                           placeholder="Nhập số điện thoại"/>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="user_address">Địa chỉ</label>
                                    <input type="text" class="form-control" id="user_address" name="user_address"
                                           placeholder="Nhập địa chỉ"/>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" style="margin-right:5px ">&nbsp;&nbsp;Lưu&nbsp;&nbsp;</button>
                        <button type="button" class="btn btn-default">Hủy bỏ</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
            <!-- /.content -->
        </div>
    </section>
    <!-- /.content -->
@endsection
