<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sanpham extends Model
{
     protected $table = "sanpham";
     public function Loaisp(){
         return $this->belongsTo('App\Loaisanpham','id_loai','id');
     }

}
