<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoaisanphamModels;
class Loaisanphamcontroller extends Controller
{
    public function index(){
        $db=LoaisanphamModels::all();
        return view('Loaisanpham.index')->with('cates',$db);
    }
    public function edit($id){
        $db=LoaisanphamModels::find($id);
        return response()->json($db);
    }
    public function create(Request $request){
        $db=new LoaisanphamModels();
        $db->TenLoai=$request->TenLoai;
        $db->save();
        return response()->json($db);
    }
    public function put(Request $request, $id){
        $db=LoaisanphamModels::find($id);
        $db->TenLoai=$request->TenLoai;
        $db->save();
        return response()->json($db);
    }
    public function delete($id){
        $db=LoaisanphamModels::destroy($id);
        return response()->json($db);
    }

}
