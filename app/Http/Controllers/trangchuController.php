<?php

namespace App\Http\Controllers;
use App\Sanpham;
use App\Loaisanpham;
use Illuminate\Http\Request;


class trangchuController extends Controller
{
    public function getIndex()
    {
        $new_spnoibat = Sanpham::where('new',1)->paginate(8);
        $new_spkhac = Sanpham::where('new',0)->paginate(4);
        return view('Pages.pagetrangchu',compact('new_spnoibat','new_spkhac'));
     }
    public function getloaisanpham($type)
    {
        $sp_theoloai = Sanpham::where('id_loai',$type)->get();
        return view('Pages.danhmuc',compact('sp_theoloai'));
     }
     public function getsanphamchitiet(Request $req)
     {
         $sanpham = Sanpham::where('id',$req->id)->first();
         return view('Pages.chitietsp',compact('sanpham'));
      }
      public function getLienhe()
     {
        return view('Pages.lienhe');
      }
      public function getGioithieu()
      {
         return view('Pages.gioithieu');
       }
public function getDangnhap()
{
   return view('Pages.dangnhap');
 }

public function getDangky()
{
   return view('Pages.dangky');
 }
}


